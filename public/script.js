kraji = [];
postneStevilke = []
krajiPostne = []

// load json, on keyup 
$(function() {
  $('#Email').on("keyup paste", validateEmail);
  $('#City').on("keyup", validateCity);

  $.getJSON("https://api.lavbic.net/kraji", function(data) {
    for (var i = 0, len = data.length; i < len; i++) {

      krajiPostne.push({
        label: data[i].kraj,
        value: data[i].postnaStevilka
      });

      kraji.push(data[i].kraj);
      postneStevilke.push(data[i].postnaStevilka)
    }
  });

});


function validateEmail() {
  var $validateIcon = $("#EmailValidate");
  var email = $("#Email").val();
  var $register = $('#Register');

  // tist iz navodil ni delal ok za vse primere
  // var re = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}$/i;
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  var isValid = re.test(String(email).toLowerCase());

  if (isValid) {
    $validateIcon.removeClass("glyphicon-remove");
    $validateIcon.addClass("glyphicon-ok");

    if ($('#PostalCode').val() != "") {
      $register.prop('disabled', false);
    }
  }
  else {
    $validateIcon.removeClass("glyphicon-ok");
    $validateIcon.addClass("glyphicon-remove");
    $register.prop('disabled', true);
  }
}

// autocomplete and validate city
function validateCity() {
  $("#City").autocomplete({
    source: krajiPostne,

    select: function(a, b) {
      $("#PostalCode").val(b.item.value);
      $("#City").val(b.item.label);
      $('#Country').val("Slovenija");
      validateEmail();
      return false;
    }
  });
}
